a = dict(button_email=False,
button_notification=False,
button_option=0,
button_state=False,
device_id="6ceceb540cf2",
device_name="AllBe1",
fitness_email=False,
fitness_notification=False,
fitness_state=False,
fitness_threshold=0,
id=1,
password_data="allbe1",
password_state=False,
pir_email=False,
pir_notification=False,
pir_state=False,
pir_threshold=15,
temperature_email=False,
temperature_notification=False,
temperature_state=True,
temperature_threshold=25,
tilt_email=False,
tilt_notification=False,
tilt_state=False,
tilt_threshold=80,
tracker_email=False,
tracker_notification=False,
tracker_state=False,
tracker_threshold=0,
user_id=1,
uv_email=False,
uv_notification=False,
uv_state=False,
uv_threshold=0)

# print a
ddict = dict()
special_k = dict()
for k in a:
    if not "_" in k or "id" in k or "name" in k:
        continue
    ddict[k.split("_")[0]] = {}

for k, v in a.items():
    if not "_" in k or "id" in k or "name" in k:
        special_k[k] = v
        continue

    ddict[k.split("_")[0]][k.split("_")[1]] = v


# print special_k
special_k1 = dict()
ddict.update(special_k)
print ddict

sdict = dict()
for k, v in ddict.items():
    if "id" in k or "name" in k:
        special_k1[k] = v
        continue
    print v
    for key, val in v.items():
        sdict["{}_{}".format(k, key)] = val

sdict.update(special_k1)
print sdict
