from gevent import monkey
monkey.patch_all()
from app import app, sockets

# socketio.run(app, host='0.0.0.0', port=8080)
# app.run(host='0.0.0.0', port=8080)


if __name__ == "__main__":
    from gevent import pywsgi
    from geventwebsocket.handler import WebSocketHandler
    server = pywsgi.WSGIServer(('', 8080), app, handler_class=WebSocketHandler)
    server.serve_forever()
