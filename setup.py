from distutils.core import setup
from distutils.dir_util import copy_tree
from glob import glob
import py2exe
import sys
import os.path


distDir = 'dist'
copy_tree(r'app/templates', os.path.join(distDir, "templates"))
copy_tree(r'app/static', os.path.join(distDir, "static"))
copy_tree(r'dep_dlls', distDir)
copy_tree(r'driver', os.path.join(distDir, "driver"))

setup(
console=[{'script': 'run.py'}],
version='1.0.0.1',
description='weballbe1',
options={"py2exe": {
    "includes": [
    "infi.devicemanager",
    "sqlalchemy.sql.default_comparator",
    "pyallbe1.ble_builder",
    "pyallbe1.ble_parser",
    "passlib.handlers",
    "passlib.handlers.sha2_crypt",
    "jinja2.ext",
    ],
    "excludes": ['Tkinter'],
    "dll_excludes": ['w9xpopen.exe', 'mswsock.dll'],
    "bundle_files": 3}},
data_files=[
    ("templates", glob(r'templates\*.*')),
    ("static", glob(r'static\*.*'))
],
)
