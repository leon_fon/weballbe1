import requests
import json
import base64
import os
working_path = os.path.dirname(os.path.abspath(__file__))
DID = '6ceceb540cf2'

playload = {'username': 'tim', 'password': '12345', 'email':'tim.sleptsov@gmail.com', 'role':'admin'}
r = requests.post('http://localhost:8080/api/users', json=(playload))#, headers={'Authorization': 'Token {}'.format(auth_token)})

if r.status_code == 200:
    print r.headers
    print r.content
else:
    print 'error {}'.format(r.status_code)
    print r.content


## get authorization token ###
# r = requests.get('http://localhost:8080/api/auth', headers={'Authorization': 'Basic {}'.format(base64.b64encode('leonidf:12345')), 'Accept': 'application/json'})
# if r.status_code == 200:
#     print r.content
#     auth_token = json.loads(r.content)['auth_token']
#     with open(working_path + r'\token.txt', 'w') as fin:
#         fin.write(auth_token)  # put the authorization token to file
# else:
#     print 'error {}'.format(r.status_code)
#     print r.content
#
# ### get dongle status ###
# with open(working_path + r'\token.txt', 'r') as fout:
#     auth_token = fout.read()
# playload = {'action': True}
# r = requests.post('http://localhost:8080/api/dongle', json=(playload), headers={'Authorization': 'Token {}'.format(auth_token)})
#
# if r.status_code == 200:
#     print r.headers
#     print r.content
# else:
#     print 'error {}'.format(r.status_code)
#     print r.content
# # #
# # scan devices ###
# with open(working_path + r'\token.txt', 'r') as fout:
#     auth_token = fout.read()
# playload = {}
# print "start scanning .. ... .. .. .. "
# r = requests.post('http://localhost:8080/api/scan', json=(playload), headers={'Authorization': 'Token {}'.format(auth_token)})
#
# if r.status_code == 200:
#     print r.headers
#     print r.content
#     devices = json.loads(r.content)
#     print type(devices)
#     print devices
# else:
#     print 'error {}'.format(r.status_code)
#     print r.content
#
# # # connect to device ###
# with open(working_path + r'\token.txt', 'r') as fout:
#     auth_token = fout.read()
# playload = {'device_id': DID}
# print "trying to connect .. .. .. .. .."
# r = requests.post('http://localhost:8080/api/connect', json=(playload), headers={'Authorization': 'Token {}'.format(auth_token)})
#
# if r.status_code == 200:
#     print r.headers
#     print r.content
# else:
#     print 'error {}'.format(r.status_code)
#     print r.content
# #
# #
# ###get settings ###
# with open(working_path + r'\token.txt', 'r') as fout:
#     auth_token = fout.read()
# playload = {'device_id': DID}
# r = requests.get('http://localhost:8080/api/settings', json=(playload), headers={'Authorization': 'Token {}'.format(auth_token)})
#
# if r.status_code == 200:
#     print r.headers
#     print "settings === ", r.content
#     settings = r.content
# else:
#     print 'error {}'.format(r.status_code)
#     print r.content

# ###set settings ###
# with open(working_path + r'\token.txt', 'r') as fout:
#     auth_token = fout.read()
#     settings = settings['PIR'] = 0
# playload = {'device_id': DID, 'settings': settings}
# r = requests.post('http://localhost:8080/api/settings', json=(playload), headers={'Authorization': 'Token {}'.format(auth_token)})
#
# if r.status_code == 200:
#     print r.headers
#     print r.content
#     settings = r.content
# else:
#     print 'error {}'.format(r.status_code)
#     print r.content
#
# ###get settings ###
# with open(working_path + r'\token.txt', 'r') as fout:
#     auth_token = fout.read()
# playload = {'device_id': DID}
# r = requests.get('http://localhost:8080/api/settings', json=(playload), headers={'Authorization': 'Token {}'.format(auth_token)})
#
# if r.status_code == 200:
#     print r.headers
#     print r.content
#     settings = r.content
# else:
#     print 'error {}'.format(r.status_code)
#     print r.content

###enable sensors ###
# with open(working_path + r'\token.txt', 'r') as fout:
#     auth_token = fout.read()
# playload = {'device_id': '6ceceb540cf2', 'measurments':True}
# r = requests.post('http://localhost:8080/api/sensors', json=(playload), headers={'Authorization': 'Token {}'.format(auth_token)})
#
# if r.status_code == 200:
#     print r.headers
#     print r.content
# else:
#     print 'error {}'.format(r.status_code)
#     print r.content

### change user status ###
# with open(working_path + r'\token.txt', 'r') as fout:
#     auth_token = fout.read()
# playload = {'first_name': 'leonid', 'last_name': 'fonaryov'}
# r = requests.put('http://localhost:8080/api/users', json=(playload), headers={'Authorization': 'Token {}'.format(auth_token)})
#
# if r.status_code == 200:
#     print r.headers
#     print r.content
# else:
#     print 'error {}'.format(r.status_code)
#     print r.content
