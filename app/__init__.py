from flask import Flask
from flask_restful import Api
from flask_sqlalchemy import SQLAlchemy
from flask_mail import Mail
from flask_sockets import Sockets
import os
import json
import logging
import logging.config
from os import sys, path


# Fetch files from app directory by default, from script directory if code is frozen
basedir = os.path.dirname(__file__)
if getattr(sys, 'frozen', False):
    basedir = os.path.dirname(sys.executable)

# Instruct Flask to load templates from the appropriate directories
app = Flask(__name__,
            template_folder=os.path.join(basedir, 'templates'),
            static_folder=os.path.join(basedir, 'static'))

logging.config.dictConfig(json.loads(open('logging.json', 'r').read()))


app.config.update(
    DEBUG = True,
    SQLALCHEMY_DATABASE_URI = r'sqlite:///{}'.format(os.path.join(os.path.abspath(basedir), 'allbe1.db')),
    THREADS_PER_PAGE = 2,
    CSRF_ENABLED = True,
    BASE_DIR = os.path.abspath(os.path.dirname(__file__)),
    CSRF_SESSION_KEY = "SuperDuperSecret",
    SECRET_KEY = "nobodyknows",
    SECURITY_PASSWORD_SALT  = "nobodyneverwillknow",
    MAIL_SERVER = 'smtp.gmail.com',
    MAIL_PORT = 465,
    MAIL_USE_TLS = False,
    MAIL_USE_SSL = True,
    MAIL_USERNAME = 'info@allbe1.com',
    MAIL_PASSWORD = 'oferozran76',
    MAIL_DEFAULT_SENDER = 'info@allbe1.com',
    SQLALCHEMY_TRACK_MODIFICATIONS = False
)
sockets = Sockets(app)

db = SQLAlchemy(app)
mail = Mail(app)
api = Api(app)

from app.common import utils, models, auth, email
db.create_all()
from app.resources.driverapi import Dongle, Scan, Connect, Settings, Sensors
from app.resources.dbapi import Authorization, Users, EmailConfirmation, Devices


api.add_resource(Dongle, '/api/dongle')
api.add_resource(Scan, '/api/scan')
api.add_resource(Connect, '/api/connect')
api.add_resource(Settings, '/api/settings')
api.add_resource(Sensors, '/api/sensors')
api.add_resource(Authorization, '/api/auth')
api.add_resource(Users, '/api/users')
api.add_resource(EmailConfirmation, '/api/emailConfirmation')
api.add_resource(Devices, '/api/devices')


@app.route('/')
def pass_app():
    # email.send_email('leonidf@crow.co.il, leon.fonar@gmail.com', 'test!!', '<p>Welcome! Thanks for signing up. Please follow this link to activate your account:</p>')
    return app.send_static_file('index.html')
