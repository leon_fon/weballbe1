from flask_mail import Message
from flask import current_app
from app import mail, app

def send_email(to, subject, template):
    with app.app_context():
        msg = Message(
            subject,
            recipients=to.split(','),
            html=template,
            sender=current_app.config['MAIL_DEFAULT_SENDER']
        )
        mail.send(msg)
