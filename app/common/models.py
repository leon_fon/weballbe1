from app import db
from passlib.apps import custom_app_context
from datetime import datetime


class User(db.Model):

    id = db.Column(db.Integer, primary_key=True)

    # User Authentication information
    username = db.Column(db.String(50), nullable=False, unique=True)
    password = db.Column(db.String(255), nullable=False, default='')
    reset_password_token = db.Column(db.String(100), nullable=False, default='')
    registred_at = db.Column(db.DateTime)

    # User Email information
    email = db.Column(db.String(255), nullable=False, unique=True)
    confirmed = db.Column(db.Boolean())
    confirmed_at = db.Column(db.DateTime)

    # User information
    first_name = db.Column(db.String(50), nullable=False, default='')
    last_name = db.Column(db.String(50), nullable=False, default='')
    avatar_url = db.Column(db.String(120), nullable=False, default='')
    role = db.Column(db.String(50), default='user')
    facebook = db.Column(db.String(120), unique=False)
    twitter = db.Column(db.String(120), unique=False)
    google_p = db.Column(db.String(120), unique=False)

    # devices
    devices = db.relationship('Device', backref='user', lazy='dynamic')

    def __init__(self, username, password, email, first_name='', last_name='', facebook='', twitter='', google_p='', role='', avatar_url=''):
        self.username = username
        self.password = custom_app_context.encrypt(password)
        self.email = email
        self.facebook = facebook
        self.twitter = twitter
        self.google_p = google_p
        self.first_name = first_name
        self.last_name = last_name
        self.avatar_url = avatar_url
        self.role = role
        self.registred_at = datetime.utcnow()
        self.confirmed = False
    
    def set_password(self, password):
        self.password = custom_app_context.encrypt(password)

    def verify_password(self, password):
        return custom_app_context.verify(password, self.password)

    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}


class Device(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    device_id = db.Column(db.String(15), nullable=False, unique=True)
    device_name = db.Column(db.String(120), unique=False)

    # sensors
    pir_state = db.Column(db.Boolean(), default=False)
    pir_notification = db.Column(db.Boolean(), default=False)
    pir_email = db.Column(db.Boolean(), default=False)
    pir_threshold = db.Column(db.Integer, default=15)
    tilt_state = db.Column(db.Boolean(), default=False)
    tilt_email = db.Column(db.Boolean(), default=False)
    tilt_notification = db.Column(db.Boolean(), default=False)
    tilt_threshold = db.Column(db.Integer, default=80)
    uv_state = db.Column(db.Boolean(), default=False)
    uv_email = db.Column(db.Boolean(), default=False)
    uv_notification = db.Column(db.Boolean(), default=False)
    uv_threshold = db.Column(db.Integer, default=0)
    temperature_state = db.Column(db.Boolean(), default=False)
    temperature_email = db.Column(db.Boolean(), default=False)
    temperature_notification = db.Column(db.Boolean(), default=False)
    temperature_min_threshold = db.Column(db.Integer, default=0)
    temperature_max_threshold = db.Column(db.Integer, default=25)
    tracker_state = db.Column(db.Boolean(), default=False)
    tracker_email = db.Column(db.Boolean(), default=False)
    tracker_notification = db.Column(db.Boolean(), default=False)
    tracker_threshold = db.Column(db.Integer, default=0)
    fitness_state = db.Column(db.Boolean(), default=False)
    fitness_email = db.Column(db.Boolean(), default=False)
    fitness_notification = db.Column(db.Boolean(), default=False)
    fitness_threshold = db.Column(db.Integer, default=0)
    password_state = db.Column(db.Boolean(), default=False)
    password_data = db.Column(db.String, default='allbe1')
    button_state = db.Column(db.Boolean(), default=False)
    button_email = db.Column(db.Boolean(), default=False)
    button_notification = db.Column(db.Boolean(), default=False)
    button_option = db.Column(db.Integer, default=0)
    button_msg = db.Column(db.String(600), default='')
    device_state = db.Column(db.Boolean(), default=True)
    device_reconnect = db.Column(db.Boolean(), default=True)
    spare_str = db.Column(db.String(200), default=True)
    spare_bool = db.Column(db.Boolean(), default=True)
    spare_int = db.Column(db.Integer(), default=True)

    def __init__(self, user_id, device_id, device_name):
        self.user_id = user_id
        self.device_id = device_id
        self.device_name = device_name

    def update_settings(self, settings):
        sdict = dict()
        special_k = dict()
        for k, v in settings.items():
            if "id" in k or "name" in k:
                special_k[k] = v
                continue
            for key, val in v.items():
                sdict["{}_{}".format(k, key)] = val

        sdict.update(special_k)
        for k, v in sdict.items():
            if hasattr(self, k):
                setattr(self, k, v)
        db.session.commit()

    def as_dict(self):
        data = {c.name: getattr(self, c.name) for c in self.__table__.columns}

        ddict = dict()
        special_k = dict()
        for k in data:
            if not "_" in k or "id" in k or "name" in k:
                continue
            ddict[k.split("_")[0]] = {}

        for k, v in data.items():
            if not "_" in k or "id" in k or "name" in k:
                special_k[k] = v
                continue

            ddict[k.split("_")[0]]["_".join(k.split('_')[1:])] = v
        # print special_k
        ddict.update(special_k)
        return ddict


class Log(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    device_id = db.Column(db.String(15), nullable=False)
    device_name = db.Column(db.String(120))
    device_event = db.Column(db.String(120))
    event_time = db.Column(db.DateTime)
    event_email = db.Column(db.Boolean())
    spare_str = db.Column(db.String(200), default=True)
    spare_bool = db.Column(db.Boolean(), default=True)
    spare_int = db.Column(db.Integer(), default=True)

    def __init__(self, device_id, device_event, event_email, user_id):
        self.device_id = device_id
        device = Device.query.filter_by(device_id=device_id).first()
        if not device:
            return False
        self.device_name = device.device_name
        self.device_event = device_event
        self.event_time = datetime.utcnow()
        self.event_email = event_email
        self.user_id = user_id
