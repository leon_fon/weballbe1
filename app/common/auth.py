from flask_httpauth import HTTPBasicAuth, HTTPTokenAuth, MultiAuth
from itsdangerous import (TimedJSONWebSignatureSerializer
                          as Serializer, BadSignature, SignatureExpired)
from itsdangerous import URLSafeTimedSerializer
from flask import g, current_app, jsonify, request, Response
from app.common.models import User
from functools import wraps

basic_auth = HTTPBasicAuth(scheme='Basic')
token_auth = HTTPTokenAuth(scheme='Token')
multi_auth = MultiAuth(basic_auth, token_auth)


@basic_auth.verify_password
def verify_password(username, password):
    # print "header in verify === {}".format(request.headers)
    if '@' in username:
        user = User.query.filter_by(email=username).first()
    else:
        user = User.query.filter_by(username=username).first()
    if not user:
        g.error = "not existing user"
        return False
    if not user.confirmed:
        g.error = "not confirmed user"
        return False

    if user.verify_password(password):
        g.user = user
        return True
    g.error = "wrong password"
    return False


@token_auth.verify_token
def verify_token(token):
    # try to authenticate with token
    user = verify_auth_token(token)
    if not user:
        return False
    g.user = user
    return True


def generate_auth_token(username, expiration=6000):
    s = Serializer(current_app.config['SECRET_KEY'], expires_in=expiration)
    return s.dumps({'username': username})


def verify_auth_token(token):
    s = Serializer(current_app.config['SECRET_KEY'])
    try:
        data = s.loads(token)
    except SignatureExpired:
        return None    # valid token, but expired
    except BadSignature:
        return None    # invalid token
    user = User.query.filter_by(username=data['username']).first()
    return user if user else None


def generate_confirmation_token(email):
    serializer = URLSafeTimedSerializer(current_app.config['SECRET_KEY'])
    return serializer.dumps(email, salt=current_app.config['SECURITY_PASSWORD_SALT'])


def verify_confirmation_token(token, expiration=3600):
    serializer = URLSafeTimedSerializer(current_app.config['SECRET_KEY'])
    try:
        email = serializer.loads(
            token,
            salt=current_app.config['SECURITY_PASSWORD_SALT'],
            max_age=expiration
        )
    except:
        return False
    return email


@basic_auth.error_handler
def auth_error():
    if 'error' in g:
        return jsonify({'error': g.error})
    return jsonify({'error': 'Unauthorized Access'})

def set_authentication_header(f):
    '''
    Decorator for flask-HTTPAuth authentication header.
    In AngularJS for prevent basic authentication popup
    :param f:
    :return:
    '''
    @wraps(f)
    def decorated(*args, **kwargs):
        res = f(*args, **kwargs)
        if 'WWW-Authenticate' in res.headers.keys():
            res.headers['WWW-Authenticate'] = res.headers['WWW-Authenticate'].replace('Basic ', 'Response ')
        return res
    return decorated
