from sys import platform
from infi.devicemanager import DeviceManager

def findport(name):
    if 'win' in platform:
        dm = DeviceManager()
        dm.root.rescan()
        try:
            device = next(i for i in dm.all_devices if name in str(i))
        except StopIteration:
            print("find_port, device name not founded in device manager")
            return None
        try:
            d = str(device.friendly_name)
            return d[d.rindex('(')+1:d.rindex(')')]
        except ValueError:
            print("find_port, quotes with COM port was not founded")
            return None
    else:
        pass

if __name__ == '__main__':
    print(findport('TI CC2540 USB CDC Serial Port'))
