from app.resources import *
from app.common.utils import findport
from pyallbe1 import Controller
from app.common.auth import token_auth
from app import sockets
from app.common.models import User, Device
from app import db, api
from geventwebsocket.exceptions import WebSocketError
import json
import time
import logging

LOGGER = logging.getLogger('driverapi')


class Dongle(Resource):

    post_args = {
        # True == reconect, False == Disconnect
        'action': fields.Bool(required=True),
    }
    @token_auth.login_required
    @use_args(post_args)
    def post(self, args):
        """
        Check if dongle exist if not trying to reconnect
        """
        # global ctrl
        LOGGER.debug(args['action'])
        if args['action']:
            ble_com_port = findport('TI CC2540 USB CDC Serial Port')
            if ble_com_port:
                LOGGER.debug("ble_ctrl.ctrl in dongle init req == {}".format(ble_ctrl.ctrl))
                if ble_ctrl.ctrl:
                    return jsonify({'dongle_status': True, 'rebuilded': False})
                else:
                    ble_ctrl.set_ctrl(Controller(ble_com_port, scan_cb=ble_ctrl.founded_allbe1, sensor_cb=ble_ctrl.sensor_data, disconnect_cb=ble_ctrl.disconnected, dongle_disconnect_cb=ble_ctrl.dongle_disconnected))
                    start_loop(ble_ctrl)
                    return jsonify({'dongle_status': True, 'rebuilded': True})
            return jsonify({'dongle_status': False})
        else:
            stop_loop()
            with ble_ctrl.get_lock():
                ble_ctrl.ctrl.stop()
                ble_ctrl.ctrl = None
            return jsonify({'dongle_status': False})

    @token_auth.login_required
    def get(self):
        #TODO check of ctrl existance is not enough
        if ble_ctrl.ctrl:
            return jsonify({'dongle_status': True})
        else:
            return jsonify({'dongle_status': False})


class Scan(Resource):

    post_args = {
        'timeout': fields.Int(required=False, missing=10),
    }

    @token_auth.login_required
    @use_args(post_args)
    def post(self, args):
        """
        Start scaning and return dictinary with all founded devices
        """
        LOGGER.debug('scan started ...')
        timeout = args['timeout']
        with ble_ctrl.get_lock():
            time.sleep(0.5)
            code = ble_ctrl.ctrl.scan()
            LOGGER.debug('exit from scan with code {}'.format(code))
            # LOGGER.debug('scan code = {}, devs = {}'.format(code, devs))
        if code == 0 :
            # devs = {k: 'AllBe1' for k, v in devs.items() if "AllBe1" in v}
            return jsonify({'devices': 'devs', 'code': code})
        else:
            return jsonify({'error': 'no devices', 'code': code})
        # return jsonify({'devices': ['allbe1 bedroom', 'allbe1 saloon', 'allbe1 kitchen']})


class Connect(Resource):
    device_args = {
        'device_id': fields.Str(required=True),
    }

    @token_auth.login_required
    @use_args(device_args)
    def post(self, args):
        """
        Connecting to specific device
        """
        device_id = args['device_id']
        LOGGER.debug("Connect called for {}".format(device_id))
        with ble_ctrl.get_lock():
            if ble_ctrl.ctrl:
                time.sleep(0.5)
                status = ble_ctrl.ctrl.connect(device_id)
                LOGGER.debug("tried to connect to {}, status == {}".format(device_id, status))
            else:
                status = False
            if status == 0 or status == 3:
                time.sleep(0.5)
                ble_ctrl.ctrl.sensor_control(args['device_id'], True)
                time.sleep(0.5)
                ble_ctrl.ctrl.start_measurement(args['device_id'], True)
                time.sleep(0.5)
                device = Device.query.filter_by(device_id=device_id).first()
                if device:
                    device.update_settings({'device':{'state': True, 'reconnect': True}})
                    db.session.commit()
                    ble_ctrl.settings = device.as_dict()
        LOGGER.debug("device {}, connected with status {}".format(device_id, status))
        return jsonify({'connection_status': status})

    @token_auth.login_required
    def put(self):
        """
        Reconnecting to all possible devices in db
        """
        if ble_ctrl.ctrl:
            with ble_ctrl.get_lock():
                time.sleep(0.5)
                ble_ctrl.ctrl.disconnect_all()
                time.sleep(0.5)
                connect_all_devices(ble_ctrl)
            return jsonify({'reconnection_status': True})
        return jsonify({'reconnection_status': False})

    @token_auth.login_required
    @use_args(device_args)
    def delete(self, args):
        """
        disconnect from specific device
        """
        device_id = args['device_id']
        LOGGER.debug("Disconnect called for {}".format(device_id))
        with ble_ctrl.get_lock():
            time.sleep(0.5)
            status = ble_ctrl.ctrl.disconnect(device_id)
            device = Device.query.filter_by(device_id=device_id).first()
            if device:
                device.update_settings({'device':{'state': False, 'reconnect': False}})
                db.session.commit()
                ble_ctrl.settings = device.as_dict()
        LOGGER.debug("Disconnect exit with code {}".format(status))
        return jsonify({'disconnect_status': status})


class Settings(Resource):
    """
    Handle Settings
    """

    set_settings = {
        'device_id': fields.Str(required=True),
        'settings': fields.Dict(required=True)
    }
    get_args = {
        'device_id': fields.Str(required=True),
    }

    @token_auth.login_required
    @use_args(set_settings)
    def post(self, args):
        """
        Send configuration
        """
        LOGGER.debug('Settings post, device id {}, settings\n{}'.format(args['device_id'], args['settings']))
        device_id = args['device_id']
        with ble_ctrl.get_lock():
            time.sleep(0.5)
            code, settings = ble_ctrl.ctrl.read_settings(device_id)
            LOGGER.debug('Settings post read settings {}, with code {}'.format(settings, code))

        if code != 0:
            return jsonify({'device_settings': 'error {}'.format(code), 'code': code})

        for k, v in args['settings'].items():
            if k in settings:
                settings[k] = v

        with ble_ctrl.get_lock():
            time.sleep(0.5)
            status = ble_ctrl.ctrl.write_settings(device_id, settings)
            LOGGER.debug('Settings post write settings, with code {}'.format(status))
        if status != 0:
            return jsonify({'device_settings': 'error {}'.format(code), 'code': code})
        g.settings = settings
        return jsonify({'op_status': status})

    @token_auth.login_required
    @use_args(get_args)
    def get(self, args):
        """
        Get device settings
        """
        # global ctrl
        device_id = args['device_id']
        with ble_ctrl.get_lock():
            time.sleep(0.5)
            code, settings = ble_ctrl.ctrl.read_settings(device_id)
            LOGGER.debug('Settings get read settings {}, with code {}'.format(settings, code))
        if code == 0:
            settings = {k: v for k, v in settings.items() if "PASSWORD" not in k}
            return jsonify({'device_settings': settings, 'code': code})
        else:
            return jsonify({'device_settings': 'error {}'.format(code), 'code': code})


class Sensors(Resource):
    """
    Handle sensors
    """

    post_args = {
        'device_id': fields.Str(required=True),
        'measurments': fields.Boolean(required=True)
    }

    @token_auth.login_required
    @use_args(post_args)
    def post(self, args):

        device_id = args['device_id']
        LOGGER.debug("device_id == {}".format(device_id))
        measurments = args['measurments']
        with ble_ctrl.get_lock():
            ble_ctrl.ctrl.sensor_control(device_id, enabled=measurments)
            status = ble_ctrl.ctrl.start_measurement(device_id, enabled=measurments)
        return jsonify({'op_status': status})


@parser.error_handler
def handle_request_parsing_error(err):
    """webargs error handler that uses Flask-RESTful's abort function to return
    a JSON error response to the client.
    """
    LOGGER.debug("erorr messages ============= {}".format(err.messages))
    abort(err.status_code, errors=err.messages)


@sockets.route('/websocket')
def ws_socket(ws):
    LOGGER.debug("web socket started {}".format(ws))

    while not ws.closed:
        item = WSQUEUE.get()
        # LOGGER.debug("item to socket {}".format(json.dumps(item)))
        try:
            ws.send(json.dumps(item))
        except WebSocketError:
            LOGGER.warning('websocket is dead')
            break
