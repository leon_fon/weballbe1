from app.resources import *
from app.common.auth import basic_auth, token_auth, generate_auth_token, set_authentication_header
from app.common.auth import generate_confirmation_token, verify_confirmation_token
from app.common.models import User, Device
from app.common.email import send_email
from flask import url_for, render_template, request
from app import db, api
import datetime
import time
import logging

LOGGER = logging.getLogger('dbapi')

class Authorization(Resource):
    """
    Perform authorization with existing user and return token
    """

    forgot_pass = {'email':fields.Str(required=True)}

    @set_authentication_header
    @basic_auth.login_required
    def get(self):
        LOGGER.debug("Authorization, get, user token request")
        #print "header == {}".format(request.headers)
        token = generate_auth_token(g.user.username)
        return jsonify({'auth_token': token.decode('ascii'), 'username': g.user.username})

    @use_args(forgot_pass)
    def put(self, args):
        email = args['email']
        LOGGER.debug("Authorization, put, forgot password for {}".format(email))
        if '@' in email:
            user = User.query.filter_by(email=email).first()
        else:
            user = User.query.filter_by(username=email).first()
        if not user:
            return jsonify({'error': "not existing user"})
        user.set_password(email.split('@')[0])
        db.session.commit()

        if not user.confirmed:
            token = generate_confirmation_token(email)
            confirm_url = url_for('emailconfirmation', token=token, _external=True)
            html = render_template('forgot_and_confirm.html', confirm_url=confirm_url, password=email.split('@')[0])
            subject = "AllBe1 password reset and email confirmation"
            send_email(user.email, subject, html)
        else:
            html = render_template('forgot.html', password=email.split('@')[0])
            subject = "AllBe1 password reset"
            send_email(user.email, subject, html)
        return jsonify({'success': 'An email with new password has been sent via email'})


class Users(Resource):
    """
    Users operations: register, delete, change
    """
    user_create_args = {
        'username': fields.Str(required=True),
        'password':fields.Str(required=True),
        'email':fields.Str(required=True),
        'facebook':fields.Str(required=False, missing=''),
        'twitter':fields.Str(required=False, missing=''),
        'google_p':fields.Str(required=False, missing=''),
        'first_name':fields.Str(required=False, missing=''),
        'last_name':fields.Str(required=False, missing=''),
        'role':fields.Str(required=False, missing='user')
    }
    user_change_args = {
        'username': fields.Str(required=False, missing=''),
        'password':fields.Str(required=False, missing=''),
        'email':fields.Str(required=False, missing=''),
        'facebook':fields.Str(required=False, missing=''),
        'twitter':fields.Str(required=False, missing=''),
        'google_p':fields.Str(required=False, missing=''),
        'first_name':fields.Str(required=False, missing=''),
        'last_name':fields.Str(required=False, missing=''),
        'role':fields.Str(required=False, missing='')
    }
    delete_args = {'username': fields.Str(required=True)}

    @use_args(user_create_args)
    def post(self, args):
        """
        Register new user and send confirmation mail to his email
        """
        username = args['username']
        password = args['password']
        email = args['email']
        facebook = args['facebook']
        twitter = args['twitter']
        google_p = args['google_p']
        first_name = args['first_name']
        last_name = args['last_name']
        role = args['role']

        if db.session.query(User.id).filter_by(username=username).scalar():
            return jsonify({'error':'This username is already exist'})
        if db.session.query(User.id).filter_by(email=email).scalar():
            return jsonify({'error':'This email is already exist'})

        user = User(username, password, email, first_name, last_name, facebook, twitter, google_p, role)
        db.session.add(user)
        db.session.commit()

        token = generate_confirmation_token(email)
        confirm_url = url_for('emailconfirmation', token=token, _external=True)
        html = render_template('mail.html', confirm_url=confirm_url)
        subject = "AllBe1 email confirmation"
        send_email(email, subject, html)
        return jsonify({'success':'A confirmation email has been sent via email'})

    @token_auth.login_required
    @use_args(user_change_args)
    def put(self, args):
        """
        If username passed the assumption that the request come from admin.
        """
        if args['username']:
            if g.user.role != 'admin':
                return jsonify({'error':'This request is possible only for admins'})
            user = User.query.filter_by(username=args['username']).first()
            if not user:
                return jsonify({'error':'The passed user ({}) is not exist!'.format(args['username'])})
        else:
            if args['role']:
                return jsonify({'error':'This field ({}) can be change only by admins'.format('role')})
            user = g.user
        for k, v in args.items():
            if v:
                setattr(user, k, v)
        db.session.commit()
        return jsonify({'success':'The user is chnged successfuly'})

    @token_auth.login_required
    @use_args(delete_args)
    def delete(self, args):
        if g.user.role != 'admin':
            return jsonify({'error':'This request is possible only for admins'})
        user = User.query.filter_by(username=args['username']).first()
        if not user:
            return jsonify({'error':'The passed user ({}) is not exist!'.format(args['username'])})
        db.session.delete(user)


class EmailConfirmation(Resource):

    email_confirm_args = {'token':fields.Str(required=True)}

    @use_args(email_confirm_args)
    def get(self, args):
        token = args['token']
        email = verify_confirmation_token(token)
        #print "email === {}".format(email)
        if not email:
            return jsonify({'confirm_status': False, 'msg':'The confirmation link is invalid or has expired.'})
        user = User.query.filter_by(email=email).first_or_404()
        #print "user === {}".format(user)

        if user.confirmed:
            return jsonify({'confirm_status': True, 'msg':'Account already confirmed. Please login.'})
        user.confirmed = True
        user.confirmed_on = datetime.datetime.now()
        db.session.add(user)
        db.session.commit()
        return jsonify({'confirm_status': True, 'msg':'You have confirmed your account. Thanks!'})

class Devices(Resource):
    new_device = {
        'username':fields.Str(required=True),
        'device_id':fields.Str(required=True),
        'device_name':fields.Str(required=False)
    }
    change_settings = {
        'device_id':fields.Str(required=True),
        'settings':fields.Dict(required=True)
    }
    get_devices = {
        'username':fields.Str(required=True),
        'device_id':fields.Str(required=False),
    }
    delete_args = {'device_id':fields.Str(required=True)}

    @token_auth.login_required
    @use_args(new_device)
    def post(self, args):
        LOGGER.debug("Devices, post, create new device {}".format(args))
        username = args['username']
        device_id = args['device_id']
        device_name = args['device_name']

        user = User.query.filter_by(username=args['username']).first()
        if not user:
            LOGGER.warning("Devices, post, The passed user ({}) is not exist!".format(args['username']))
            return jsonify({'error':'The passed user ({}) is not exist!'.format(args['username'])})

        if db.session.query(Device.id).filter_by(device_id=device_id).scalar():
            LOGGER.warning("Devices, post, This device is already exist {}".format(args['device_id']))
            return jsonify({'error':'This device is already exist'})

        device = Device(user.id, device_id, device_name)
        db.session.add(device)
        db.session.commit()
        return jsonify({'success':'The device is created successfuly'})

    @token_auth.login_required
    @use_args(get_devices)
    def get(self, args):
        """
        Get all devices of user
        """
        username = args['username']
        LOGGER.debug("Devices, get, get all devices for user {}".format(username))
        user = User.query.filter_by(username=args['username']).first()
        if not user:
            LOGGER.warning("Devices, get, The passed user ({}) is not exist!".format(args['username']))
            return jsonify({'error':'The passed user ({}) is not exist!'.format(args['username'])})

        if not args['device_id']:
            devices = Device.query.filter_by(user_id=user.id).all()
            if not devices:
                LOGGER.warning("Devices, get, The passed user ({}) have no device".format(args['username']))
                return jsonify({'error':'The passed user ({}) have no device'.format(args['username'])})

            dict_devices = [d.as_dict() for d in devices]
            return jsonify({'devices':dict_devices})

        device = Device.query.filter_by(device_id=args['device_id']).first()
        if not device:
            LOGGER.warning("Devices, get, The passed device id ({}) is not exist!".format(args['device_id']))
            return jsonify({'error':'The passed device id ({}) is not exist!'.format(args['device_id'])})

        return jsonify({'device':device.as_dict()})

    @token_auth.login_required
    @use_args(change_settings)
    def put(self, args):

        device_id = args['device_id']
        device = Device.query.filter_by(device_id=device_id).first()
        if not device:
            return jsonify({'error':'The passed device ({}) is not exist!'.format(args['device_id'])})
        LOGGER.debug("Devices, PUT, args['settings'] == {}".format(args['settings']))
        device.update_settings(args['settings'])
        # for k, v in args['settings'].items():
        #     if k in device:
        #         setattr(device, k, v)
        # db.session.commit()

        with ble_ctrl.get_lock():
            time.sleep(0.5)
            code, settings = ble_ctrl.ctrl.read_settings(device_id)
        if code != 0 or not settings:
            return jsonify({'error': 'error during read settings {}, with code {}'.format(settings, code), 'code': code})
        LOGGER.debug("Devices, PUT, settings from the device ==== {}".format(settings))
        #print "device.as_dict()  ============= ", device.as_dict()
        for k, v in device.as_dict().items():
            #print k
            if k == "pir":
                settings['PIR'] = int(v['state'])
            elif k == "tilt":
                if v['state']:
                    settings['MOVEMENTS'] = int(v['threshold'])
                else:
                    settings['MOVEMENTS'] = int(v['state'])
            elif k == "uv":
                settings['UV'] = int(v['state'])
            elif k == "temperature":
                settings['TEMPERATURE'] = int(v['state'])
            elif k == "tracker":
                settings['TRACKER'] = int(v['state'])
            elif k == "fitness":
                settings['FITNESS'] = int(v['state'])
            elif k == "password":
                if v['state']:
                    settings['PASSWORD'] = bytearray(str(v['data']).rjust(6, '0'), 'utf-8')
                else:
                    settings['PASSWORD'] = b'\x00\x00\x00\x00\x00\x00'
        # LOGGER.debug("Devices, PUT, settings after modify == {}".format(settings))
        # settings = {}
        # for k, v in settings.items():
        #     if k in settings:
        #         settings[k] = v
        with ble_ctrl.get_lock():
            time.sleep(0.5)
            status = ble_ctrl.ctrl.write_settings(device_id, settings)
            # time.sleep(2)
            # code, settings = ble_ctrl.ctrl.read_settings(device_id)
            # print 'REREAD SETTINGS', settings

        if status != 0:
            return jsonify({'error': 'error during set device settings {}'.format(code), 'code': code})
        ble_ctrl.settings = device.as_dict()

        return jsonify({'success':'The settings are chaged successfuly'})

    @token_auth.login_required
    @use_args(delete_args)
    def delete(self, args):
        LOGGER.debug("Devices, delete, delete device {}".format(args['device_id']))
        device = Device.query.filter_by(device_id=args['device_id']).first()
        if not device:
            return jsonify({'error':'The passed device ({}) is not exist!'.format(args['device_id'])})
        with ble_ctrl.get_lock():
            time.sleep(0.5)
            ble_ctrl.ctrl.disconnect(args['device_id'])
            db.session.delete(device)
            db.session.commit()
            ble_ctrl.settings.pop(args['device_id'], None)
        return jsonify({'success':'The device is deleted successfuly'})


@parser.error_handler
def handle_request_parsing_error(err):
    """webargs error handler that uses Flask-RESTful's abort function to return
    a JSON error response to the client.
    """
    LOGGER.warning("erorr messages ============= {}".format(err.messages))
    abort(err.status_code, errors=err.messages)
