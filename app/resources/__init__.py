import time
import binascii
import gevent
from flask_restful import Resource, abort
from flask import jsonify, g, render_template
from webargs import fields, validate
from webargs.flaskparser import use_args, use_kwargs, parser
from app.common.email import send_email
from app.common.models import User, Device, Log
from app.common.utils import findport
from pyallbe1 import Controller
from threading import Lock
from queue import Queue
from functools import wraps
from app import db, api
import logging

LOGGER = logging.getLogger('weballbe1')


WSQUEUE = Queue()
RSSI_QUEUE = Queue()
RECONNECT_INTERVAL = 30
LOOP_ACTIVE = True
LOOP_HANDLE = None


def singleton(class_):
    instances = {}

    def getinstance(*args, **kwargs):
        if class_ not in instances:
            instances[class_] = class_(*args, **kwargs)
        return instances[class_]
    return getinstance


@singleton
class BLEGlobCtrl(object):

    def __init__(self, ctrl_object=None):
        if not ctrl_object:
            ble_com_port = findport('TI CC2540 USB CDC Serial Port')
            if ble_com_port:
                self.ctrl_obj = Controller(ble_com_port,
                 scan_cb=self.founded_allbe1,
                  sensor_cb=self.sensor_data,
                   disconnect_cb=self.disconnected,
                    dongle_disconnect_cb=self.dongle_disconnected,
                    rssi_cb=self.rssi)
                self.ctrl_obj.disconnect_all()
                devices = db.session.query(Device).all()
                for dev in devices:
                    dev.device_state = False
                db.session.commit()
                LOGGER.debug("Dongle is up!")
            else:
                self.ctrl_obj = None
        else:
            self.ctrl_obj = ctrl_object
        self.lock = Lock()
        self._settings = {}
        self._sensors_rssi = dict()

    def set_ctrl(self, ctrl):
        self.ctrl = ctrl

    def ctrl(self):
        return self.ctrl

    def get_lock(self):
        return self.lock

    def set_sensors_rssi(self, data):
        with self.lock:
            self._sensors_rssi[data['addr']] = data['rssi']

    def rssi(self, msg):
        self.set_sensors_rssi(msg)

    def _handle_notifications(self, msg, k, v, settings, user):
        keys_dict = {'pir': 'PIR_VALUE', 'uv':'UV_INDEX'}
        if v['state']:
            if msg['data'][keys_dict[k]] > v['threshold']:
                log = Log(settings['device_id'], k, v['email'], settings['user_id'])
                db.session.add(log)
                db.session.commit()
                if v['email']:
                        # html = render_template('mail_alarm.html', sensor="PIR")
                    html = "<h3>Alarm!</h3><p>Alarm was occured at {} !</p><p>From device: {} !</p>".format(k, settings['device_name'])
                    send_email(user.email, "AllBe1 Sensor Alarm", html)
                if v['notification']:
                    WSQUEUE.put({'type': 'notification', 'sensor': '{}'.format(k), 'value': msg['data'][keys_dict[k]], 'device':settings})

    def sensor_data(self, msg):
        # RSSI_QUEUE.put(msg['addr'])
        msg['data'].update({'RSSI': self._sensors_rssi.get(msg['addr'], None)})
        # print("msg before queue = {}".format(msg))
        WSQUEUE.put({'type': 'sensor_data', 'message': msg})
        settings = self.settings.get(msg['addr'], None)
        if not settings:
            return
        user_id = settings['user_id']
        user = User.query.filter_by(id=user_id).first()
        for k, v in settings.items():
            if k in ['pir', 'uv']:
                self._handle_notifications(msg, k, v, settings, user)
            elif k == 'tilt' and v['state']:
                if msg['data']['MOVEMENTS_ALERT']:
                    log = Log(settings['device_id'], k, v['email'], settings['user_id'])
                    db.session.add(log)
                    db.session.commit()
                    if v['email']:
                            # html = render_template('mail_alarm.html', sensor="PIR")
                        html = "<h3>Alarm!</h3><p>Alarm was occured at {} sensor!</p><p>From device: {} !</p>".format(k, settings['device_name'])
                        send_email(user.email, "AllBe1 Sensor Alarm", html)
                    if v['notification']:
                            WSQUEUE.put({'type': 'notification', 'sensor': '{}'.format(k), 'value': True, 'device':settings})
            elif k == 'temperature' and v['state']:
                if not (v['min_threshold'] <= msg['data']['TEMPERATURE'] <= v['max_threshold']):
                    log = Log(settings['device_id'], k, v['email'], settings['user_id'])
                    db.session.add(log)
                    db.session.commit()
                    if v['email']:
                        # html = render_template('mail_alarm.html', sensor="PIR")
                        html = "<h3>Alarm!</h3><p>Alarm was occured at {} !</p><p>From device: {} !</p>".format(k, settings['device_name'])
                        send_email(user.email, "AllBe1 Sensor Alarm", html)
                    if v['notification']:
                        WSQUEUE.put({'type': 'notification', 'sensor': '{}'.format(k), 'value': msg['data']['TEMPERATURE'], 'device':settings})
            elif k == "tracker" and v['state'] and msg['data']['RSSI']:
                if msg['data']['RSSI'] < v['threshold']:
                    log = Log(settings['device_id'], k, v['email'], settings['user_id'])
                    db.session.add(log)
                    db.session.commit()
                    if v['email']:
                        html = "<h3>Alarm!</h3><p>Alarm was occured at {} !</p><p>From device: {} !</p>".format(k, settings['device_name'])
                        send_email(user.email, "AllBe1 Sensor Alarm", html)
                    if v['notification']:
                        WSQUEUE.put({'type': 'notification', 'sensor': '{}'.format(k), 'value': msg['data']['RSSI'], 'device':settings})
            elif k == 'button' and v['state']:
                if msg['data']['KEY']:
                    log = Log(settings['device_id'], k, v['email'], settings['user_id'])
                    db.session.add(log)
                    db.session.commit()
                    if v['email']:
                            # html = render_template('mail_alarm.html', sensor="PIR")
                        html = "<h3>Panic!</h3><p>panic message: {} !</p><p>From device: {} !</p>".format(v['msg'], settings['device_name'])
                        send_email(user.email, "AllBe1 Panic Alarm", html)
                    if v['notification']:
                            WSQUEUE.put({'type': 'notification', 'sensor': '{}'.format(k), 'value': True, 'device':settings})

    @staticmethod
    def founded_allbe1(msg):
        LOGGER.debug('I found {}'.format(msg))
        if 'AllBe1' in str(binascii.b2a_qp(msg['data'])):
            WSQUEUE.put({'type': 'scan', 'device': [msg['addr'], "Allbe1"]})

    def disconnected(self, msg):
        time.sleep(2)
        settings = self.settings.get(msg, None)
        if settings:
            user_id = settings['user_id']
            user = User.query.filter_by(id=user_id).first()
            device = Device.query.filter_by(device_id=msg).first()
            device.update_settings({'device':{'state': False}})
            db.session.commit()
            self.settings = device.as_dict()
            log = Log(msg, 'disconnected', True, settings['user_id'])
            db.session.add(log)
            db.session.commit()
            LOGGER.debug('Disconnected {}'.format(msg))
            WSQUEUE.put({'type': 'disconnect', 'message': msg})
            if settings['device']['reconnect'] and settings['tracker']['state']:
                if settings['tracker']['email']:
                    html = "<h3>Alarm!</h3><p>{} device disconnected</p>".format(settings['device_name'])
                    send_email(user.email, "AllBe1 Alarm", html)
                if settings['tracker']['notification']:
                    WSQUEUE.put({'type': 'notification', 'sensor': 'tracker', 'value': True, 'device':settings})

    def dongle_disconnected(self):
        LOGGER.debug('Disconnected dongle')
        stop_loop()
        self.ctrl_obj = None
        WSQUEUE.put({'type': 'dongle_disconnect', 'message': 'disconnected'})

    @property
    def ctrl(self):
        return self.ctrl_obj

    @ctrl.setter
    def ctrl(self, ctrl):
        self.ctrl_obj = ctrl

    @property
    def settings(self):
        return self._settings

    @settings.setter
    def settings(self, setting):
        self._settings[setting['device_id']] = setting
        # print("i got settings and now the === {}".format(self.settings))


def connect_all_devices(ble_ctrl):
    ctrl = ble_ctrl.ctrl
    db.session.expire_all()
    devices = db.session.query(Device).all()
    for dev in devices:
        if dev.device_state is False and dev.device_reconnect is True: 
            ble_ctrl.settings = dev.as_dict()
            with ble_ctrl.get_lock():
                time.sleep(0.5)
                status = ctrl.connect(dev.device_id)
                LOGGER.debug("tried to connect to {}, status == {}".format(dev.device_id, status))
                if status == 0 or status == 3:
                    dev.device_state = True
                    time.sleep(0.5)
                    ble_ctrl.ctrl.sensor_control(dev.device_id, True)
                    time.sleep(0.5)
                    ble_ctrl.ctrl.start_measurement(dev.device_id, True)
                    time.sleep(0.5)
    db.session.commit()


def reconnect_loop(ble_ctrl):
    tick = RECONNECT_INTERVAL-1
    while LOOP_ACTIVE:
        tick += 1
        if tick % RECONNECT_INTERVAL == 0:
            connect_all_devices(ble_ctrl)
            tick = 0
        while True:
            try:
                addr = RSSI_QUEUE.get_nowait()
                with ble_ctrl.get_lock():
                    ble_ctrl.ctrl.getRSSI(addr)
                gevent.sleep(1)
            except:
                break
        gevent.sleep(1)


def start_loop(ble_ctrl):
    global LOOP_ACTIVE
    global LOOP_HANDLE
    LOOP_ACTIVE = True
    LOOP_HANDLE = gevent.Greenlet.spawn(reconnect_loop, ble_ctrl)
    LOGGER.debug("LOOP_HANDLE in start loop == {}".format(LOOP_HANDLE))


def stop_loop():
    global LOOP_ACTIVE
    global LOOP_HANDLE
    LOOP_ACTIVE = False
    LOGGER.debug("LOOP_HANDLE in stop loop == {}".format(LOOP_HANDLE))
    if LOOP_HANDLE:
        gevent.Greenlet.join(LOOP_HANDLE)
        LOOP_HANDLE = None


ble_ctrl = BLEGlobCtrl()

if ble_ctrl.ctrl:
    start_loop(ble_ctrl)
else:
    pass
    #  TODO: reconnect to dongle looop
