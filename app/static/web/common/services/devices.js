'use strict';

(function () {
  function DevicesService($http) {
    console.log("devices init");
      var self = this;
      self.url = {
        devices: '/api/devices',
      };

      self.devices = {};
      self.debug = false;

      self.setDebug = function(state){
        self.debug = state;
      }
      self.getDebug = function(state){
        return self.debug;
      }

      self.addDevice = function(username, device_id, device_name) {
        var queue = {
          url: self.url.devices,
          method: 'POST',
          data: {username:username, device_id:device_id, device_name:device_name}
        };
        return $http(queue);
      };

      self.getDevices = function(username, device_id) {
        var queue = {
          url: self.url.devices,
          method: 'GET',
          params: {username:username, device_id:device_id}
        };
        return $http(queue);
      };

      self.changeSettings = function(device_id, settings) {
        var queue = {
          url: self.url.devices,
          method: 'PUT',
          data: {device_id:device_id, settings:settings}
        };
        return $http(queue);
      };

      self.delDevice = function(device_id) {
        var queue = {
          url: self.url.devices,
          method: 'DELETE',
          params: {device_id:device_id}
        };
        return $http(queue);
      };
//      $interval(self.getStatus, 3000);

  };
  angular.module('allbe1').service('DevicesService', ['$http', DevicesService]);
}());
