'use strict';

(function () {
  function BLEService($http, $interval) {
    console.log("ble init");
    var self = this;
    self.url = {
      dongle: '/api/dongle',
      scan: '/api/scan',
      connect: '/api/connect',
      sensors: '/api/sensors',
      settings: 'api/settings'
    };
    self.dongle = {state:false};
    self.getLocalDongleState = function(){
      return self.dongle['state'];
    }
    self.setLocalDongleState = function(state){
      self.dongle['state'] = state;
      console.log('dongle state set to ', state);
    }

    // self.getDeviceList = function() {
    //   return ['device1', 'device2'];
    // }

    // self.setAuthToken = function(token) {
    //   $http.defaults.headers.common.Authorization = 'Token ' + token;
    // }

    self.setDongleState = function(state) {
      var queue = {
        url: self.url.dongle,
        method: 'POST',
        data: {action:state}
      };
      return $http(queue);

    };

    self.startScan = function() {
      var queue = {
        url: self.url.scan,
        method: 'POST'
      };
      return $http(queue);
    };

    self.connect = function(device_id) {
      var queue = {
        url: self.url.connect,
        method: 'POST',
        data: {device_id:device_id}
      };
      return $http(queue);
    };

    self.reconnect = function() {
      var queue = {
        url: self.url.connect,
        method: 'PUT',
      };
      var req = $http(queue);
      req.success(function(result){
        self.dongle['reconnected'] = true;
      })
      req.error(function(){
        console.error('http error occurred in reconnect');
      })
    };

    self.disconnect = function(device_id) {
      var queue = {
        url: self.url.connect,
        method: 'DELETE',
        params: {device_id:device_id}
      };
      return $http(queue);
    };

    self.getSettings = function(device_id) {
      var queue = {
        url: self.url.settings,
        method: 'GET',
        params: {device_id:device_id}
      };
      return $http(queue);
    };

    self.setSettings = function(device_id, settings) {
      var queue = {
        url: self.url.settings,
        method: 'POST',
        params: {device_id:device_id, settings:settings}
      };
      var req = $http(queue);
      req.success(function (result) {
        return result.data['op_status']
      });
      req.error(function(){
        console.error('http error occured in setSettings');
      });
    };

    self.setSensorsState = function(device_id, state) {
      var queue = {
        url: self.url.sensors,
        method: 'POST',
        data: {device_id:device_id, measurments:state}
      };
      return $http(queue);
    };

//      $interval(self.getStatus, 3000);

  };
  angular.module('allbe1').service('BLEService', ['$http', '$interval', BLEService]);
}());
