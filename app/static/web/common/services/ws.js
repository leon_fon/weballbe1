(function() {
  function WSService() {
    console.log("websocket init");
    var self = this;
    self.callbacks = {};

//   To Enable when Server code will ready

   var ws = new WebSocket("ws://localhost:8080/websocket");

   ws.onopen = function(){
       console.log("Socket has been opened!");
   };

   ws.onmessage = function(message) {
     var msg = JSON.parse(message.data);
     var msg_type = msg['type'];
     if (msg_type in self.callbacks) {
       cb = self.callbacks[msg_type];
       cb(msg);
     }
   };

   self.add_callback = function(keyword, cb) {
     self.callbacks[keyword] = cb;
   }

  };
  angular.module('allbe1').service('WSService', [WSService]);
}());
