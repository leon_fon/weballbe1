'use strict';

(function () {
  function UsersService($http, $interval, authTokenVal, usernameVal) {
    console.log("User service init");
      var self = this;
      self.login = true;
      self.username = usernameVal;
      self.url = {
        auth: '/api/auth',
        users: '/api/users',
        email: '/api/emailConfirmation'
      };

      self.setUsername = function(username){
        self.username = username;
      }
      self.getUsername = function(){
        return self.username;
      }
      self.setLogin = function(login){
        // console.log("loggin setted to ", login);
        self.login = login;
      }
      self.getLogin = function(){
        // console.log("login getted ", self.login);
        return self.login;
      }

      self.setAuthToken = function(token) {
        $http.defaults.headers.common.Authorization = 'Token ' + token;
      };

      if (authTokenVal){
        self.setAuthToken(authTokenVal);
      }

      self.auth = function(username, password) {
        var req = {
          url: self.url.auth,
          method: 'GET',
          headers: {'Authorization':'Basic ' + window.btoa(username+":"+password)}
        };
        return $http(req);
      };
      
      self.forgotPass = function(email) {
        var req = {
          url: self.url.auth,
          method: 'PUT',
          data: {email:email}
        };
        return $http(req);
      };

      self.authBase64 = function(uspass) {
        var req = {
          url: self.url.auth,
          method: 'GET',
          headers: {'Authorization':'Basic ' + uspass}
        };
        return $http(req);
      };

      self.regUser = function(user_obj) {
        var req = {
          url: self.url.users,
          method: 'POST',
          data: user_obj
        };
        console.log("req in regUser  ==  ", req)
        return $http(req);
      };

      self.editUser = function(user_obj) {
        var req = {
          url: self.url.users,
          method: 'PUT',
          data: user_obj
        };
        var req = $http(req);
        req.success(function (result) {
          if ('error' in result){
            console.error(result['error']);
            return false;
          }
          return true;
        });
        req.error(function(){
          console.error('http error occured in editUser');
        });
      };

      self.deleteUser = function(username) {
        var req = {
          url: self.url.users,
          method: 'DELETE',
          data: {username:username}
        };
        var req = $http(req);
        req.success(function (result) {
          if ('error' in result){
            console.error(result['error']);
            return false;
          }
          return true;
        });
        req.error(function(){
          console.error('http error occured in deleteUser');
        });
      };

//      $interval(self.getStatus, 3000);

  }
  angular.module('allbe1').service('UsersService', ['$http', '$interval', 'authTokenVal', 'usernameVal', UsersService]);
}());
