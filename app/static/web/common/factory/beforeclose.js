'use strict';

(function () {
  function beforeClose($rootScope, $window) {
    $window.onbeforeunload = function (e) {
        alert('huy factory');
        var confirmation = {};
        var event = $rootScope.$broadcast('onBeforeUnload', confirmation);
        if (event.defaultPrevented) {
            return confirmation.message;
        }
    };

    $window.onunload = function () {
        alert('huy unl;oad factory');
        $rootScope.$broadcast('onUnload');
    };
    return {};
  }
  angular.module('allbe1').factory('beforeClose', ['$rootScope', '$window', beforeClose]);
}());
