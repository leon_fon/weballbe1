'use strict';

(function () {
  function remember($cookies) {
    console.log("remember init");
      return function(name, values) {
        // var cookie = name + '=';
        // cookie += values + ';';
        // cookie += 'expires=' + date.toString() + ';';
        var date = new Date();
        date.setDate(date.getDate() + 365);
        $cookies.putObject(name, values, {'expires': date})
        // document.cookie = cookie;
      }
  }
  angular.module('allbe1').factory('$remember', ['$cookies', remember]);
}());
