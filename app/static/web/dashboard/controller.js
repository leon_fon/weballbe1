(function() {
  function DashboardController(BLEService, DevicesService, WSService, UsersService, $timeout, $location, $scope) {
    console.log("DashboardController started");
    var self = this;
    self.ble = BLEService;
    self.ws = WSService;
    self.devs = DevicesService;
    self.user = UsersService;
    self.scannedDevices = {};
    self.userDevices = {};
    self.scanning = false;
    self.connecting_first = {};
    self.connecting = {};
    self.connecting_s = {};
    self.disconnecting = {};
    self.sensors = {};
    self.notification = '';
    self.debug = false;
    $scope.$watch('self.ble.getLocalDongleState()', function(nval){
      self.dongleState = nval;
      init();
    })
    $scope.$watch('self.devs.getDebug()', function(nval){
      self.debug = nval;
    })

    if (self.user.getLogin()){
      $location.url('/login');
    }

    self.onSensorData = function(message) {
      $scope.$apply(function() {
        var d_id = message.message.addr
         self.sensors[d_id] = message.message.data;
         if (d_id in self.userDevices){
            if (!self.userDevices[d_id][1]){
            self.userDevices[d_id][1] = true;
            self.connecting_first[d_id] = false;
            }
         }
      });
    }

    self.checkConnection = function(){
      for(d_id in self.userDevices){
        if (!self.userDevices[d_id][1]){
          self.connecting_first[d_id] = false;
        }
      }
    }

    self.onDisconnect = function(message) {
      $scope.$apply(function() {
        var deviceName = self.userDevices[message.message][0];
        self.userDevices[message.message] = [deviceName, false];
      });
    }

    self.onScan = function(message) {
      $scope.$apply(function() {
        console.log("on scan message == ", message);
        self.scannedDevices[message.device[0]] = message.device;
      });
    }

    self.startScanning = function(){
      self.scanning = true;
      var req = self.ble.startScan();
      req.success(function (result) {
        self.scanning = false;
      });
      req.error(function(){
        self.scanning = false;
        console.error('http error occured in startScan');
      });
    }

    self.connectScanned = function(deviceId){
      if ((deviceId[0] in self.devs.devices)){
        self.connect(self.devs.devices[deviceId[0]]);
        delete self.scannedDevices[deviceId[0]];
      }
      else{
        self.connecting_s[deviceId[0]] = true;
        var req = self.ble.connect(deviceId[0]);
        req.success(function (result) {
          if(result['connection_status'] == 0 || result['connection_status'] == 3){
              reqd = self.devs.addDevice(self.user.username, deviceId[0], deviceId[1])
              reqd.success(function () {
                reqds = self.devs.changeSettings(deviceId[0], {temperature:{state:true}})
                reqds.success(function (result) {
                  reqget = self.devs.getDevices(self.user.username, deviceId[0])
                  reqget.success(function (result){
                    self.userDevices[deviceId[0]] = [deviceId[1], true];
                    delete self.scannedDevices[deviceId[0]];
                    self.connecting_s[deviceId[0]] = false;
                    self.devs.devices[result['device']['device_id']] = result['device'];
                    // self.connecting_first[deviceId[0]] = true;
                  })
                });
                reqds.error(function(){
                  console.error('http error occured in changeSettings');
                });
              })
              reqd.error(function (){
                console.error('Failed to add device to db');
              });
          }
          else {
            self.connecting_s[deviceId[0]] = false;
            console.error('connection failed for device', deviceId);
          }
        });
        req.error(function(){
          self.connecting_s[deviceId[0]] = false;
          console.error('http error occured in connect');
        });
      }
    }

    self.connect = function(deviceData){
      self.connecting[deviceData['device_id']] = true;
      var req = self.ble.connect(deviceData['device_id']);
      req.success(function (result) {
        if(result['connection_status'] == 0 || result['connection_status'] == 3){
          var set_settings = {};
          _.each(deviceData, function(value, key){
            if (!key.includes('id')){
              set_settings[key] = value;
            }
          });
          reqds = self.devs.changeSettings(deviceData['device_id'], set_settings)
          reqds.success(function (result) {
            if ('error' in result){
              console.error('error occured during changeSettings: ', result['error']);
            }
            else{
                self.userDevices[deviceData['device_id']] = [deviceData['device_name'], true];
                // self.connecting_first[deviceData['device_id']] = true;
            }
            self.connecting[deviceData['device_id']] = false;
          });
          reqds.error(function(){
            console.error('http error occured in changeSettings');
            self.connecting[deviceData['device_id']] = false;
          });
        }
        else {
          self.connecting[deviceData['device_id']] = false;
          self.userDevices[deviceData['device_id']] = [deviceData['device_name'], false];
          console.error('connection failed for device', deviceData);
        }
      });
      req.error(function(){
        self.connecting[deviceData['device_id']] = false;
        console.error('http error occured in connect');
      });
    }

    self.disconnect = function(deviceId){
      self.disconnecting[deviceId] = true;
      var req = self.ble.disconnect(deviceId);
      req.success(function (result) {
        self.userDevices[deviceId][1] = false;
        self.disconnecting[deviceId] = false;
      });
      req.error(function(){
        self.disconnecting[deviceId] = false;
        console.error('http error occured in disconnect');
      });
    }

    self.deleteDevice = function(deviceId){
      var req = self.devs.delDevice(deviceId);
      req.success(function (result){
        if ('error' in result){
          console.error('error occured during delete device', result['error']);
        }
        else {
          delete self.devs.devices[deviceId];
          delete self.userDevices[deviceId];
        }
      })
    }

    self.routeToSettings = function(deviceId){
      // console.log("self.devs.devices[deviceId]['password']['state'] == ", self.devs.devices[deviceId]['password']['state'])
      $location.path('/settings/').search({deviceId: deviceId});
    }

    var addCBs = function(){
      self.ws.add_callback('sensor_data', self.onSensorData);
      self.ws.add_callback('disconnect', self.onDisconnect);
      self.ws.add_callback('scan', self.onScan);
    }

    var init = function(){
      if (self.dongleState){
        if (!_.isEmpty(self.devs.devices)){
          var devices = self.devs.devices;
          for(d in devices){
            self.userDevices[devices[d]['device_id']] = [devices[d]['device_name'], false]
            self.connecting_first[devices[d]['device_id']] = true;
          }
          $timeout(self.checkConnection, 10000);
          addCBs();
        }
        else {
          var req = self.devs.getDevices(self.user.username, '');
          req.success(function(result){
            if ('error' in result){
              addCBs();
              console.error('error in dashboard init', result['error']);
            }
            else{
              var devices = result['devices'];
              for (d in devices){
                self.devs.devices[devices[d]['device_id']] = devices[d];
                self.userDevices[devices[d]['device_id']] = [devices[d]['device_name'], false]
                self.connecting_first[devices[d]['device_id']] = true;
              }
              $timeout(self.checkConnection, 10000);
              addCBs();
            }
          });
          req.error(function() {
            console.error("http error occured in getDevices");
          })
        }
      }
    }
    init();

  };
  angular.module('allbe1')
    .controller('DashboardController', ['BLEService', 'DevicesService', 'WSService', 'UsersService', '$timeout', '$location', '$scope', DashboardController]);
}());
