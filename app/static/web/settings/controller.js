(function () {
  function SettingsController(UsersService, BLEService, DevicesService, $scope, $location, WSService) {
    console.log("settings controller start");
    var self = this;
    self.deviceId = $location.search().deviceId;
    self.ble = BLEService;
    self.devs = DevicesService;
    self.user = UsersService;
    self.ws = WSService;
    self.sensors = {};
    self.updating = false;
    self.pirCustom = false;
    self.deviceUpdated = false;
    self.dongleState = false;
    // self.settings = {};
    self.deviceData = {};
    $scope.password = '';
    $scope.$watch('self.ble.getLocalDongleState()', function (nval) {
      self.dongleState = nval;
    })

    $scope.$on('$locationChangeStart', function (event) {

      if (JSON.stringify(self.deviceData) != JSON.stringify(self.devs.devices[self.deviceId])) {
        var answer = confirm("You have unsaved chages, are you sure you want to leave this page?")
        if (!answer) {
          event.preventDefault();
        }
      }
    });


    // self.settingsParams = {pir:{}, temperature:{}, tilt:{}, button:{}, tracker:{}};

    // for (k in self.devs.devices[self.deviceId]) {
    //             self.deviceData[k] = self.devs.devices[self.deviceId][k];
    //           }
    self.deviceData = JSON.parse(JSON.stringify(self.devs.devices[self.deviceId]));
    
    self.tempSlider = { floor: -20, ceil: 40 };
    if (!self.deviceData) {
      // console.log("self.user.username == ", self.user.username);
      var req = self.devs.getDevices(self.user.username, self.deviceId);
      req.success(function (result) {
        if ('error' in result) {
          console.error('error in settings init', result['error']);
        }
        else {
          self.deviceData = result['device'];
          // self.spreadData(result['device']);
          // console.error('device == ', result['device']);
          // for (d in devices){
          //   if (devices[d]['device_id'] === self.deviceId){
          //     self.deviceData = devices[d];
          //     self.spreadData(devices[d]);
          //     break;
          //   }
          // }
        }
      });
    }

    self.onSensorData = function (message) {
      $scope.$apply(function () {
        var d_id = message.message.addr;
        if (d_id === self.deviceId) {
          self.sensors = message.message.data;
        }
      });
      // console.log(message.message.data)
      // console.log(message.message.addr)
      // console.log(self.sensors);
    }

    self.ws.add_callback('sensor_data', self.onSensorData);

    self.setPirCustom = function (state) {
      self.pirCustom = state;
    }

    self.getPirCustom = function () {
      // console.log(self.deviceData)
      var res = !(self.deviceData['pir'].threshold === 15 || self.deviceData['pir'].threshold === 22 || self.deviceData['pir'].threshold === 30);
      // console.log("getPirCustom result == ", res);
      // console.log("self.deviceData['pir'].threshold == ", self.deviceData['pir'].threshold);
      return res;
    }

    self.swapSettingState = function (key) {
      self.deviceData[key].state = !self.deviceData[key].state;
    }

    self.getIncPirThreshold = function () {
      var val = parseInt(self.deviceData['pir'].threshold, 10) + 1;
      return '' + val;
    }

    // self.spreadData = function(deviceData){
    //   if (!deviceData){
    //     return;
    //   }
    //   _.each(deviceData, function(value, key) {
    //     if (key === "pir"){
    //       self.settings['pir'] = value;
    //     }
    //     else if (key === "tilt") {
    //       self.settings['tilt'] = value;
    //     }
    //     else if (key === "temperature"){
    //       self.settings['temperature'] = value;
    //     }
    //     else if (key === "button"){
    //       self.settings['button'] = value;
    //     }
    //     else if (key === "tracker") {
    //       self.settings['tracker'] = value;
    //     }
    //     else if (key === "device_name") {
    //       self.settings['device_name'] = value;
    //     }
    //   });
    // }
    // self.spreadData(self.deviceData);

    self.update = function () {
      self.updating = true;
      // console.log("self.deviceData  === ", self.deviceData);

      // console.log("self.settingsParams  === ", self.settingsParams);
      req = self.devs.changeSettings(self.deviceId, self.deviceData);

      req.success(function (result) {
        if ('error' in result) {
          self.deviceUpdated = false;
          console.error('error occured during changeSettings: ', result['error']);
        }
        else {
          if ("text" in self.deviceData.button) {
            self.deviceData.button.option += ':' + self.deviceData.button.text;
          }
          for (k in self.deviceData) {
            self.devs.devices[self.deviceId][k] = self.deviceData[k];
          }
          self.deviceUpdated = true;
        }
        self.updating = false;
      });
      req.error(function () {
        self.deviceUpdated = false;
        console.error('devices are failed to update');
        self.updating = false;
      });

    }

    // var getSettings = function(){
    //   var req = self.ble.getSettings(self.deviceId);
    //   req.success(function (result) {
    //     console.log('device_settings  ==  ', result['device_settings']);
    //     _.each(result['device_settings'], function(value, key) {
    //         console.log(key, value);
    //         });
    //     self.settings = result['device_settings'];
    //   });
    //   req.error(function(){
    //     console.log('http error occured in getSettings');
    //   });
    // }();

  };
  angular.module('allbe1')
    .controller('SettingsController', ['UsersService', 'BLEService', 'DevicesService', '$scope', '$location', 'WSService', SettingsController]);
}());
