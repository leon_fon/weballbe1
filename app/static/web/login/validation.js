(function() {
angular.module('allbe1').directive('validUsername', function () {
    return {
        require: 'ngModel',
        link: function (scope, elm, attrs, ctrl) {
            ctrl.$parsers.unshift(function (viewValue, $scope) {
                // Any way to read the results of a "required" angular validator here?
                var isBlank = viewValue === '';
                var invalidChars = !isBlank && !/^[A-z0-9]+$/.test(viewValue);
                var invalidLen = !isBlank && !invalidChars && (viewValue.length < 5 || viewValue.length > 20);
                ctrl.$setValidity('isBlank', !isBlank);
                ctrl.$setValidity('invalidChars', !invalidChars);
                ctrl.$setValidity('invalidLen', !invalidLen);
                scope.usernameGood = !isBlank && !invalidChars && !invalidLen;
                if (scope.usernameGood){
                    // console.log("username setted to ", viewValue);
                    return viewValue;
                }
            })
        }
    }
}).directive('validPassword', function () {
    return {
        require: 'ngModel',
        link: function (scope, elm, attrs, ctrl) {
            ctrl.$parsers.unshift(function (viewValue, $scope) {
                var isBlank = viewValue === '';
                var invalidLen = !isBlank && (viewValue.length < 8 || viewValue.length > 20);
                var isWeak = !isBlank && !invalidLen && !/(?=.*[a-z])(?=.*[^a-zA-Z])/.test(viewValue);
                ctrl.$setValidity('isBlank', !isBlank);
                ctrl.$setValidity('isWeak', !isWeak);
                ctrl.$setValidity('invalidLen', !invalidLen);
                scope.passwordGood = !isBlank && !isWeak && !invalidLen;
                if (scope.passwordGood){
                    // console.log("password setted to ", viewValue);
                    return viewValue;
                }
            })
        }
    }
}).directive('validPasswordC', function () {
    return {
        require: 'ngModel',
        link: function (scope, elm, attrs, ctrl) {
            ctrl.$parsers.unshift(function (viewValue, $scope) {
                var isBlank = viewValue === '';
                var noMatch = viewValue != scope.myform.password.$viewValue;
                ctrl.$setValidity('isBlank', !isBlank);
                ctrl.$setValidity('noMatch', !noMatch);
                scope.passwordCGood = !isBlank && !noMatch;
                if (scope.passwordCGood){
                    // console.log("password_c setted to ", viewValue);
                    return viewValue;
                }
            })
        }
    }
}).directive('validEmail', function () {
    return {
        require: 'ngModel',
        link: function (scope, elm, attrs, ctrl) {
            ctrl.$parsers.unshift(function (viewValue, $scope) {
                var isBlank = viewValue === '';
                var invalidEmail = !isBlank && !/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(viewValue);
                ctrl.$setValidity('isBlank', !isBlank);
                ctrl.$setValidity('invalidEmail', !invalidEmail);
                scope.emailGood = !isBlank && !invalidEmail;
                if (scope.emailGood){
                    // console.log("email setted to ", viewValue);
                    return viewValue;
                }
            })
        }
    }
}).directive('validAllbePass', function () {
    return {
        require: 'ngModel',
        link: function (scope, elm, attrs, ctrl) {
            ctrl.$parsers.unshift(function (viewValue, $scope) {
                console.log("inside validAllbePass");
                var isBlank = viewValue === '';
                var invalidLen = !isBlank && (viewValue.length != 6);
                var isWeak = !isBlank && !invalidLen && !/[\x00-\x7F]/.test(viewValue);
                ctrl.$setValidity('isBlank', !isBlank);
                ctrl.$setValidity('isWeak', !isWeak);
                ctrl.$setValidity('invalidLen', !invalidLen);
                scope.passwordGood = !isBlank && !isWeak && !invalidLen;
                if (scope.passwordGood){
                    console.log("password goood ", viewValue);
                    return viewValue;
                }
            })
        }
    }
});
}());
