(function() {
  function LoginController(UsersService, BLEService, $remember, $location, $scope) {
    console.log("login controller start");
    var self = this;
    self.user = UsersService;
    self.ble = BLEService;
    self.error = false;
    self.user.setLogin(true);
    self.signingup = false;
    self.regMsg = '';
    self.forgotMsg = '';
    self.state = 'login';
    self.reseting_pswd = false;
    // $scope.username = '';
    // $scope.password = '';
    // $scope.password_c = '';
    // $scope.email = '';
    $scope.formAllGood = function () {
        return ($scope.usernameGood && $scope.passwordGood && $scope.passwordCGood && $scope.emailGood);
    }


    self.setState = function(state){
      self.state = state;
      if (state === 'signup'){
        $location.url('/signup');
      }
      else if (state === 'login'){
        $location.url('/login');
      }
      // console.log('state changed to ', self.state);
    }
    self.getState = function(){
      return self.state;
    }

    self.login = function(username, password, remember){
      self.state = 'login'
      self.error = false;
      req = self.user.auth(username, password);
      req.success(function (result) {
        // console.log("result in auth == ", result);
        username = result['username'];
        self.user.setAuthToken(result['auth_token']);
        // self.ble.setAuthToken(result['auth_token']);
        self.user.username = username
        // console.log("username ", username, "set into self.user.username");
        // console.log("self.user.username in ligin == ", self.user.username);
        $remember('username', {'username':username, 'remember':remember, 'uspass': window.btoa(username+":"+password)});
        self.user.setLogin(false);
        $location.url('/');
      });
      req.error(function(response){
        console.error('http error occured in auth with response: ', response);
        self.error = response['error'];
      });
    };

    self.forgotPass = function(){
      req = self.user.forgotPass($scope.username)
      self.reseting_pswd = true;
      req.success(function(result){
        // console.log('forgot password result == ', result)
         self.reseting_pswd = false;
        if ('error' in result){
          self.forgotMsg = result['error'];
        }
        else{
          self.forgotMsg = result['success'];
        }
      });
      req.error(function(response){
        self.reseting_pswd = false;
        console.error('http error occured in forgot password with response: ', response);
        self.error = response['error'];
      });
    }

    self.signup = function(){
      self.state = 'signup';
      self.signingup = true;
      var userObj = {username:$scope.username, password:$scope.password, email:$scope.email};
      // console.log("userObj == ", userObj);
      var req = self.user.regUser(userObj);
      req.success(function (result) {
        if ('success' in result){
          self.regMsg = result['success'];
          self.signingup = false;
        }
        else {
          self.error = 'Something gone wrong'
          self.signingup = false;
        }
      });
      req.error(function(){
        self.error = 'Something gone wrong'
        self.signingup = false;
        console.error('http error occured in regUser');
      });
    }
  };
  angular.module('allbe1')
    .controller('LoginController', ['UsersService', 'BLEService', '$remember', '$location', '$scope', LoginController]);
}());
