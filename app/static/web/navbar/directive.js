(function() {
  function NavBarController(BLEService, UsersService, $location, $forget, $scope, $timeout, WSService, DevicesService){
    console.log("navbar controller started");
      var self = this;
      self.ble = BLEService;
      self.user = UsersService;
      self.ws = WSService;
      self.devs = DevicesService;
      self.dongleState = false;
      self.notifications = [];
      // console.log("Users server login in nav bar == ", self.user.getLogin());
      self.swapDebug = function(){
        self.devs.setDebug(!self.devs.getDebug());
      }

      self.onNotification = function(message) {
      $scope.$apply(function() {
        var userMsg = {};
        userMsg['name'] = message.device['device_name'];
        if (message.sensor === 'pir'){
            userMsg['msg'] = "Burgler Alarm!";
        }
        else if (message.sensor === 'tilt'){
          userMsg['msg'] = "Movement Detection!";
        }
        else if (message.sensor === 'temperature'){
          userMsg['msg'] = "Temperature " + message.value + " exceeded the limit";
        }
        else if (message.sensor === 'button'){
          userMsg['msg'] = "Multi-button pressed, with text" + message.device['button']['msg'];
        }
        else if (message.sensor === 'tracker'){
          userMsg['msg'] = "Out Of Range!";
        }
        
        self.notifications.push(userMsg);
        self.playAudio()
        $timeout(function(){
          self.notifications.shift();
        }, 10000);
      });
    }

    self.playAudio = function() {
        var audio = new Audio('static/audio/system-fault.mp3');
        audio.play();
    };

      self.onDongleDisconnect = function(message) {
        $scope.$apply(function() {
          console.log("Dongle disconnect, message == ", message);
          self.dongleState = false;
          self.ble.setLocalDongleState(false);
        });
      }
      self.ws.add_callback('dongle_disconnect', self.onDongleDisconnect);
      self.ws.add_callback('notification', self.onNotification);

      self.goHome = function(){
        $location.url('/');
      }

      self.setDongleState = function(state){
        var req = self.ble.setDongleState(state);
        req.success(function (result) {
          if (result['dongle_status']){
            if (result['rebuilded']){
              self.ble.reconnect();
            }
          }
          console.log("result in setDongleState === ", result);
          self.dongleState = result['dongle_status'];
          self.ble.setLocalDongleState(result['dongle_status']);
        });
        req.error(function(){
          console.error('http error occurred in setDongleState');
        });
      };

      $scope.$watch('self.user.getLogin()', function(nval){
        if (nval == false){
          self.setDongleState(true);
        }
      })

      self.logout = function(){
        self.user.username = '';
        self.user.setAuthToken('');
        $forget('username');
        $location.url('login');
      };

      self.profile = function(){
        $location.url('profile');
      }
  };
  angular.module('allbe1').controller("NavBarController", ["BLEService", "UsersService", "$location", "$forget", "$scope", "$timeout", "WSService", "DevicesService", NavBarController]);

  function navBar() {
    return {
      restrict: 'E',
      templateUrl: 'static/web/navbar/template.html',
      controller:NavBarController,
      controllerAs: 'self'
    }
  };
  angular.module('allbe1').directive('navBar', navBar);
}());
