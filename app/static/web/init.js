// allbe1.factory('InitFactory', function(){
//   console.log('init factory init');
//   var self = this;
//   self.login = true;
//
//   self.setLogin = function(state){
//     self.login = state;
//     console.log("login state == ", state);
//   }
//   self.getLogin = function(){
//     return self.login;
//   }
// });
allbe1.value('authTokenVal', '')
allbe1.value('usernameVal', '')
initApp = function(){
  var initInjector = angular.injector(["allbe1","ng", "ngCookies"]);
  var $http = initInjector.get("$http");
  var $cookies = initInjector.get("$cookies");
  var $remember = initInjector.get("$remember");
  var $forget = initInjector.get("$forget");
  var $q = initInjector.get("$q");
  // console.log(initInjector.has("InitFactory"));
  // var InitFactory = initInjector.get("InitFactory");
  var user = initInjector.get("UsersService");


  // console.log("$cookies.getObject('username') === ", $cookies.getObject('username'));
  if (!$cookies.getObject('username')){
      allbe1.value('login', true);
      // InitFactory.setLogin(true);
      return $q.resolve(true);
  }
  else{
    var cookie = $cookies.getObject('username');
    if (!cookie['remember']){
      $forget('username');
      allbe1.value('login', true);
      // InitFactory.setLogin(true);
      return $q.resolve(true);
    }
    else{
      allbe1.value('usernameVal', cookie['username'])
      return user.authBase64(cookie['uspass'])
      .success(function (result) {
        allbe1.value('authTokenVal', result['auth_token'])
        // console.log('auth success');
        allbe1.value('login', false);
        // InitFactory.setLogin(false);
      })
      .error(function(response){
        $forget('username');
        allbe1.value('login', true);
        // InitFactory.setLogin(true);
        return $q.resolve(true);
        console.error('http error occured in auth with response: ', response);
      });
    }
  }
}
function bootstrapApplication() {
  angular.element(document).ready(function() {
      angular.bootstrap(document, ["allbe1"]);
  });
}

initApp().then(bootstrapApplication);


allbe1.config(['$routeProvider',
  function($routeProvider) {
    $routeProvider.
    when('/login', {
      templateUrl: 'static/web/login/template.html/',
      controller: 'LoginController',
      controllerAs: 'self',
      routeName: 'login'
    }).
    when('/signup', {
      templateUrl: 'static/web/login/template_signup.html/',
      controller: 'LoginController',
      controllerAs: 'self',
      routeName: 'signup'
    }).
    when('/settings', {
      templateUrl: 'static/web/settings/template.html/',
      controller: 'SettingsController',
      controllerAs: 'self',
      routeName: 'settings'
    }).
    when('/', {
      templateUrl: 'static/web/dashboard/template.html/',
      controller: 'DashboardController',
      controllerAs: 'self',
      routeName: 'dashboard'
    }).
    otherwise({
      redirectTo: '/test/',
    });
  }
]);


allbe1.run(function($rootScope, $cookies, $location, $window, $forget, UsersService, login) {
  console.log("run started");
  var user = UsersService;
  // var initf = InitFactory;
  user.setLogin(login);
  // console.log("$cookies.getObject('username') === ", $cookies.getObject('username'));
  // console.log("initf.getLogin() == ", initf.getLogin());

  if (login){
    $location.url('/login');
  }
  else{
    $location.url('/');
  }
  $rootScope.$on('$routeChangeStart', function(evt, next, currentRoute) {
    $rootScope.isActive = next.$$route.routeName;
  });
});

allbe1.controller("ManageController", ["UsersService", "BLEService", function(UsersService, BLEService){
  var self = this;
  self.user = UsersService;
  self.ble = BLEService;
}]);

allbe1.directive('stringToNumber', function() {
  return {
    require: 'ngModel',
    link: function(scope, element, attrs, ngModel) {
      ngModel.$parsers.push(function(value) {
        return '' + value;
      });
      ngModel.$formatters.push(function(value) {
        return parseFloat(value);
      });
    }
  };
});

allbe1.directive('convertToNumber', function() {
  return {
    require: 'ngModel',
    link: function(scope, element, attrs, ngModel) {
      ngModel.$parsers.push(function(val) {
        return val != null ? parseInt(val, 10) : null;
      });
      ngModel.$formatters.push(function(val) {
        return val != null ? '' + val : null;
      });
    }
  };
});

// allbe1.controller("NavBarController", ["BLEService", "UsersService", "$scope", "$window", "$location", "$forget", function(BLEService, UsersService, $scope, $window, $location, $forget){
// console.log("navbar controller started");
//   var self = this;
//   self.ble = BLEService;
//   self.user = UsersService;
//   self.dongleState = false;
//   console.log("Users server login in nav bar == ", self.user.getLogin());
//
//   self.goHome = function(){
//     $location.url('/');
//   }
//
//   self.setDongleState = function(state){
//     var req = self.ble.setDongleState(state);
//     req.success(function (result) {
//       console.log("result in setDongleState === ", result);
//       self.dongleState = result['dongle_status'];
//       self.ble.dongleState = result['dongle_status'];
//     });
//     req.error(function(){
//       console.log('http error occurred in setDongleState');
//     });
//   };
//   if (self.user.getLogin() == false){
//     self.setDongleState(true);
//   }
//
//   self.logout = function(){
//     self.user.username = '';
//     self.user.setAuthToken('');
//     $forget('username');
//     $location.url('login');
//   };
//
//   self.profile = function(){
//     $location.url('profile');
//   }
// }]);
